-- MySQL dump 10.13  Distrib 8.0.23, for Linux (x86_64)
--
-- Host: localhost    Database: project
-- ------------------------------------------------------
-- Server version	8.0.23-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `beer`
--

DROP TABLE IF EXISTS `beer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `beer` (
  `id` int NOT NULL AUTO_INCREMENT,
  `brewery_id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` enum('stout','IPA','amber ale','pale ale','specialty ale','kolsch','lager','pilsner','seltzer','radler','european','other') DEFAULT NULL,
  `alcohol_percentage` decimal(2,1) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` text,
  `gluten_free` tinyint(1) DEFAULT NULL,
  `price` decimal(5,2) DEFAULT NULL,
  `amount` int DEFAULT NULL,
  `container_type` enum('can','bottle','growler','keg') DEFAULT NULL,
  `click_num` int DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `brewery_id` (`brewery_id`),
  CONSTRAINT `beer_ibfk_1` FOREIGN KEY (`brewery_id`) REFERENCES `breweries` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `beer`
--

LOCK TABLES `beer` WRITE;
/*!40000 ALTER TABLE `beer` DISABLE KEYS */;
INSERT INTO `beer` VALUES (1,1,'Bluebeary Ale','specialty ale',5.0,'bluebeary_bottle.jpg','A wheat beer with added natural blueberry flavour. Subtle berry notes on the nose and palate, a well-balanced ale with a refreshing finish',0,2.74,355,'bottle',68,'2021-04-15 19:49:10','2021-04-15 19:49:10'),(2,1,'Lamp Lighter Amber Ale','amber ale',5.2,'amber_ale_can.jpg','Easy drinking ale brewed using traditional UK malts to impart a deep red hue with notes of caramel, raisin and a hint of roasted chocolate',0,2.12,355,'can',78,'2021-04-15 19:49:10','2021-04-15 19:49:10'),(3,1,'Portager Bohemian Pilsner','pilsner',5.3,'portager_bohemian.jpg','Brewed using Bohemian Pilsner malt and Saaz hops. A crisp, clean and refreshing lager with distinct hop aromas of floral and subtle spice',0,2.44,355,'bottle',30,'2021-04-15 19:49:10','2021-04-15 19:49:10'),(4,1,'Cold Brew Coffee Stout','stout',7.0,'coffee_stout.jpg','Bold coffee flavour is balanced with a chocolate-forward profile from the dark malts. A dark, expressive and full-bodied stout, to be enjoyed at any time of your day!',0,2.74,355,'can',80,'2021-04-15 19:49:10','2021-04-15 19:49:10'),(5,2,'Stir Stick Stout','stout',6.0,'stir_stick.jpg','Robust with ruby highlights and a generous addition of locally roasted fair trade, organic Ethiopian Yirgacheffe coffee',0,4.09,473,'can',75,'2021-04-15 19:49:10','2021-04-15 19:49:10'),(6,2,'Little Scrapper IPA','IPA',6.0,'little_scrapper.jpg','A firm, toasted malt presence forms the background for all these hops lending balance so it’s not just a one note symphony',0,3.95,473,'can',80,'2021-04-15 19:49:10','2021-04-15 19:49:10'),(7,2,'Bulldog Amber Ale','amber ale',5.4,'bulldog_amber.jpg','British crystal malts give this beer its distinctly deep amber body and nutty aroma that persist throughout the glass',0,2.32,341,'bottle',72,'2021-04-15 19:49:10','2021-04-15 19:49:10'),(8,2,'St. James Pale Ale','pale ale',4.8,'st_james.jpg','St. James Pale Ale is a medium bodied blonde brew that’s soft on the palette with a malty finish',0,2.14,341,'bottle',103,'2021-04-15 19:49:10','2021-04-15 19:49:10'),(9,3,'Juicii IPA','IPA',7.0,'juicii.jpg','A hoppy, tropical, and juicy New England IPA (7%) double dry-hopped with El Dorado, Vic Secret, lupulin-enriched Mosaic & Citra',0,4.35,473,'can',140,'2021-04-15 19:49:10','2021-04-15 19:49:10'),(10,3,'Waves Juicy Pale Ale','pale ale',5.0,'waves_juicy.jpg','A tropical, breezy, and juicy New England Pale Ale (5%) dry-hopped with waves of El Dorado & Citra',0,4.09,473,'can',150,'2021-04-15 19:49:10','2021-04-15 19:49:10'),(11,3,'Fog Machine Cashmere Double IPA','IPA',8.0,'for_machine.jpg','A super hoppy single-hop Double IPA brewed and double dry-hopped with 100% Cashmere!',0,4.89,473,'can',80,'2021-04-15 19:49:10','2021-04-15 19:49:10'),(12,3,'Throwback West Coast IPA','IPA',7.0,'throwback_IPA.jpg','A dank & hoppy West Coast IPA (7%) brewed with Golden Promise malt and aggressively dry-hopped with Centennial, Cryo Simcoe, and Cryo Amarillo',0,4.59,473,'can',100,'2021-04-15 19:49:10','2021-04-15 19:49:10'),(13,4,'1919 Belgian Pale Ale','pale ale',5.0,'1919_IPA.jpg','1919 is an easy-drinking everyday Belgian Pale Ale',0,2.40,473,'can',93,'2021-04-15 19:49:10','2021-04-15 19:49:10'),(14,4,'Golden Ale','specialty ale',6.5,'golden_ale.jpg','A CRISP GOLDEN ALE IN OUR BELGIAN STYLE MADE WITH MANITOBA HONEY',0,2.40,473,'can',114,'2021-04-15 19:49:10','2021-04-15 19:49:10'),(15,4,'Summer Lager','lager',5.0,'summer_lager.jpg','A SUMMER LAGER WITH A REFRESHING OUTLOOK',0,2.40,473,'can',104,'2021-04-15 19:49:10','2021-04-15 19:49:10'),(16,4,'Belgian IPA','IPA',6.5,'belgian_ipa.jpg','ENGINEERED TO SHOWCASE NEW WORLD HOPS IN OUR BELGIAN STYLE',0,2.40,473,'can',120,'2021-04-15 19:49:10','2021-04-15 19:49:10'),(17,5,'Lime Seltzer','seltzer',5.0,'lime_seltzer.jpg','Lime flavour Seltzer',0,3.59,473,'can',72,'2021-04-15 19:49:10','2021-04-15 19:49:10'),(18,5,'Cranberry Crush Radler','radler',3.5,'cranberr_cruch_radler.jpg','A beautiful pink-hued, effervescent radler, infused with tart cranberry juice',0,3.79,473,'can',88,'2021-04-15 19:49:10','2021-04-15 19:49:10'),(19,5,'Portage and Main IPA','IPA',6.5,'portagage_and_main.jpg','All aboard with this west coast-style IPA, featuring high-quality malted barley and whole west coast and Manitoba flower hops',0,3.99,473,'can',110,'2021-04-15 19:49:10','2021-04-15 19:49:10'),(20,5,'Northern Lite','other',4.0,'northern_lite.jpg','Northern Lite is a clean, crisp beer which finishes dry. A perfect beach or cabin companion. Made using 100% barley we strive to create the best tasting light beer possible',0,2.98,473,'can',103,'2021-04-15 19:49:10','2021-04-15 19:49:10');
/*!40000 ALTER TABLE `beer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `breweries`
--

DROP TABLE IF EXISTS `breweries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `breweries` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `street` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `postal_code` varchar(255) NOT NULL,
  `province` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `open_time_mon` int DEFAULT NULL,
  `close_time_mon` int DEFAULT NULL,
  `open_time_tue` int DEFAULT NULL,
  `close_time_tue` int DEFAULT NULL,
  `open_time_wed` int DEFAULT NULL,
  `close_time_wed` int DEFAULT NULL,
  `open_time_thu` int DEFAULT NULL,
  `close_time_thu` int DEFAULT NULL,
  `open_time_fri` int DEFAULT NULL,
  `close_time_fri` int DEFAULT NULL,
  `open_time_sat` int DEFAULT NULL,
  `close_time_sat` int DEFAULT NULL,
  `open_time_sun` int DEFAULT NULL,
  `close_time_sun` int DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `breweries`
--

LOCK TABLES `breweries` WRITE;
/*!40000 ALTER TABLE `breweries` DISABLE KEYS */;
INSERT INTO `breweries` VALUES (1,'Trans Canada Brewing Co.','1-1290 Kenaston Boulevard','Winnipeg','R3P 0R7','Manitoba','Canada',NULL,NULL,NULL,NULL,1400,2200,1400,2200,1400,2200,1400,2200,1400,1900,'trans_canada.jpg','2046662337','info@tcb.beer','2021-04-15 19:16:39','2021-04-15 19:16:39'),(2,'Half Pints Brewing','550 Roseberry Street','Winnipeg','R3H 0T1','Manitoba','Canada',1100,1800,1100,1800,1100,1800,1100,1800,1100,1800,1100,1800,1100,1800,'half_pints.jpg','2048327468','info@halfpintsbrewing.com','2021-04-15 19:16:39','2021-04-15 19:16:39'),(3,'Kilter Brewing Co.','450 Rue Deschambault','Winnipeg','R2H 0K1','Manitoba','Canada',NULL,NULL,NULL,NULL,1200,2000,1200,2000,1200,2000,1200,2000,NULL,NULL,'kilter.jpg',NULL,'hello@kilterbrewing.co','2021-04-15 19:16:39','2021-04-15 19:16:39'),(4,'Little Brown Jug Brewing Co.','336 William Avenue','Winnipeg','R3A 0H7','Manitoba','Canada',NULL,NULL,1200,1800,1200,1800,1200,2000,1200,2000,1200,2000,NULL,NULL,'little_brown_jug.jpg','2045000441','beer@littlebrownjug.ca','2021-04-15 19:16:39','2021-04-15 19:16:39'),(5,'Fort Garry Brewing Co.','130 Lowson Crescent','Winnipeg','R3P 2H8','Manitoba','Canada',900,1700,900,1700,900,1700,900,1700,900,1800,NULL,NULL,NULL,NULL,'fort_garry.jpg','2044873678','info@fortgarry.com','2021-04-15 19:16:39','2021-04-15 19:16:39');
/*!40000 ALTER TABLE `breweries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reviews` (
  `id` int NOT NULL AUTO_INCREMENT,
  `beer_id` int NOT NULL,
  `user_id` int NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `star_num` int DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `beer_id` (`beer_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `reviews_ibfk_1` FOREIGN KEY (`beer_id`) REFERENCES `beer` (`id`),
  CONSTRAINT `reviews_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` VALUES (1,1,1,'Interesting','Bluebeary Ale was one of the first craft beers I had ever tried. In my opinion, it is more of a summer beer as it is fruity yet refreshing. I prefer bottle over can, but I stole this from Shans beer fridge so can’t complain. Cheers',3,'2021-04-15 20:53:50','2021-04-15 20:53:50'),(2,14,2,'Honey Taste!','I really enjoyed this brew! Great work! Loved the honey taste',4,'2021-04-15 20:53:50','2021-04-15 20:53:50'),(3,20,3,'Taste of Winnipeg','Clean, light, non descript beer',3,'2021-04-15 20:53:50','2021-04-15 20:53:50'),(4,13,4,'Belgian Classic','Nice clean crisp and modestly sweet with a definite malt flavour',5,'2021-04-15 20:53:50','2021-04-15 20:53:50'),(5,8,5,'Great Pale Ale!','I can’t believe I’ve never checked this one in',4,'2021-04-15 20:53:50','2021-04-15 20:53:50');
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `last_name` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `street` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Henry','Thierry','Emirates','Fassword','1111111111','striker@example.com','123 Arsenal Street','Winnipeg','R3B 2E9','Manitoba','Canada','2021-04-15 20:24:41','2021-04-15 20:24:41'),(2,'Pirlo','Andrea','OldLady','Iassword','2222222222','regista@example.com','234 Juventus Street','Winnipeg','R3B 2E8','Manitoba','Canada','2021-04-15 20:24:41','2021-04-15 20:24:41'),(3,'Carragher','Jamie','CenterBack','Lassword','3333333333','kop@example.com','345 Anfield Street','Winnipeg','R3B 2E7','Manitoba','Canada','2021-04-15 20:24:41','2021-04-15 20:24:41'),(4,'Neville','Gary','Fullback','Massword','4444444444','red_devil@example.com','456 Old Trafford Street','Winnipeg','R3B 2E6','Manitoba','Canada','2021-04-15 20:24:41','2021-04-15 20:24:41'),(5,'Busquets','Sergio','anchorman','Sassword','5555555555','barcelona@example.com','413 Camp Nou Street','Winnipeg','R3B 2E5','Manitoba','Canada','2021-04-15 20:24:41','2021-04-15 20:24:41');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-15 21:04:14
