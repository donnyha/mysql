# MYSQL Project

## Step 1

A conceptual data model of your entire database, showing all major entities, their
relationship and cardinality.

Field and key names are not required in this diagram. Use Word
or OpenOffice to create this diagram.

## Step 2

A logical data model of your entire database, showing all normalized tables, their various
keys, and cardinality. Use Word or OpenOffice to create this diagram.

## Step 3

An Entity Relationship Diagram of your entire database. This is a physical data model and
should include all field names, data types, keys, etc. You may use MySQL Workbench,
Word, or OpenOffice to create this diagram.

## Step 4

Create all your tables in MySQL. populate your primary table with at least 20 records. Be
sure to populate all other tables with at least five records.

```sql
-- Creating table breweries
CREATE TABLE breweries (
id INT NOT NULL AUTO_INCREMENT,
name VARCHAR(255) NOT NULL,
street VARCHAR(255) NOT NULL,
city VARCHAR(255) NOT NULL,
postal_code VARCHAR(255) NOT NULL,
province VARCHAR(255) NOT NULL,
country VARCHAR(255) NOT NULL,
open_time_mon INT,
close_time_mon INT,
open_time_tue INT,
close_time_tue INT,
open_time_wed INT,
close_time_wed INT,
open_time_thu INT,
close_time_thu INT,
open_time_fri INT,
close_time_fri INT,
open_time_sat INT,
close_time_sat INT,
open_time_sun INT,
close_time_sun INT,
image VARCHAR(255),
phone_number VARCHAR(255),
email VARCHAR(255),
created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (id)
);

-- Insert values
INSERT INTO
breweries
(id,
name,
street,
city,
postal_code,
province,
country,
open_time_mon,
close_time_mon,
open_time_tue,
close_time_tue,
open_time_wed,
close_time_wed,
open_time_thu,
close_time_thu,
open_time_fri,
close_time_fri,
open_time_sat,
close_time_sat,
open_time_sun,
close_time_sun,
image,
phone_number,
email
)
VALUES
-- 1. Trans Canada
(1,
'Trans Canada Brewing Co.', '1-1290 Kenaston Boulevard', 'Winnipeg', 'R3P 0R7', 'Manitoba', 'Canada',
null, -- mon
null,
null, -- tue
null,
1400, -- wed
2200,
1400, -- thur
2200,
1400, -- fri
2200,
1400, -- sat
2200,
1400, -- sun
1900,
'trans_canada.jpg', 2046662337, 'info@tcb.beer'
),

-- 2. Half Pints
(2,
'Half Pints Brewing', '550 Roseberry Street', 'Winnipeg', 'R3H 0T1', 'Manitoba', 'Canada',
1100, -- mon
1800,
1100, -- tue
1800,
1100, -- wed
1800,
1100, -- thur
1800,
1100, -- fri
1800,
1100, -- sat
1800,
1100, -- sun
1800,
'half_pints.jpg', 2048327468, 'info@halfpintsbrewing.com'
),

-- 3. Kilter
(3,
'Kilter Brewing Co.', '450 Rue Deschambault', 'Winnipeg', 'R2H 0K1', 'Manitoba', 'Canada',
null, -- mon
null,
null, -- tue
null,
1200, -- wed
2000,
1200, -- thur
2000,
1200, -- fri
2000,
1200, -- sat
2000,
null, -- sun
null,
'kilter.jpg', null, 'hello@kilterbrewing.co'
),

-- 4. Lake of the Woods
(4,
'Little Brown Jug Brewing Co.', '336 William Avenue', 'Winnipeg', 'R3A 0H7', 'Manitoba', 'Canada',
null, -- mon
null,
1200, -- tue
1800,
1200, -- wed
1800,
1200, -- thur
2000,
1200, -- fri
2000,
1200, -- sat
2000,
null, -- sun
null,
'little_brown_jug.jpg', 2045000441, 'beer@littlebrownjug.ca'
),

-- 5. Fort Garry
(5,
'Fort Garry Brewing Co.', '130 Lowson Crescent', 'Winnipeg', 'R3P 2H8', 'Manitoba', 'Canada',
0900, -- mon
1700,
0900, -- tue
1700,
0900, -- wed
1700,
0900, -- thur
1700,
0900, -- fri
1800,
null, -- sat
null,
null, -- sun
null,
'fort_garry.jpg', 2044873678, 'info@fortgarry.com'
);

-- Update
UPDATE
breweries
SET
country = 'Canada'
WHERE
id = 1;

-- Create table beer
CREATE TABLE beer (
id INT NOT NULL AUTO_INCREMENT,
brewery_id INT NOT NULL,
name VARCHAR(255) NOT NULL,
type ENUM ('stout', 'IPA', 'amber ale', 'pale ale', 'specialty ale', 'kolsch', 'lager', 'pilsner', 'seltzer', 'radler', 'european', 'other'),
alcohol_percentage DECIMAL (2,1),
image VARCHAR(255),
description TEXT,
gluten_free BOOLEAN,
price DECIMAL (5,2),
amount INT,
container_type ENUM ('can', 'bottle', 'growler', 'keg'),
click_num INT,
created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (id),
FOREIGN KEY (brewery_id) REFERENCES breweries(id)
);

-- ALTER TABLE beer MODIFY alcohol_percentage DECIMAL (2,1);

-- Insert values
INSERT INTO
beer
(brewery_id, name, type, alcohol_percentage, image, description, gluten_free, price, amount, container_type, click_num)
VALUES
-- 1. Trans Canada
(1, 'Bluebeary Ale', 'specialty ale', 5.0, 'bluebeary_bottle.jpg', 'A wheat beer with added natural blueberry flavour. Subtle berry notes on the nose and palate, a well-balanced ale with a refreshing finish', 0, 2.74, 355, 'bottle', 68),

(1, 'Lamp Lighter Amber Ale', 'amber ale', 5.2, 'amber_ale_can.jpg', 'Easy drinking ale brewed using traditional UK malts to impart a deep red hue with notes of caramel, raisin and a hint of roasted chocolate', 0, 2.12, 355, 'can', 78),

(1, 'Portager Bohemian Pilsner', 'pilsner', 5.3, 'portager_bohemian.jpg', 'Brewed using Bohemian Pilsner malt and Saaz hops. A crisp, clean and refreshing lager with distinct hop aromas of floral and subtle spice', 0, 2.44, 355, 'bottle', 30),

(1, 'Cold Brew Coffee Stout', 'stout', 7.0, 'coffee_stout.jpg', 'Bold coffee flavour is balanced with a chocolate-forward profile from the dark malts. A dark, expressive and full-bodied stout, to be enjoyed at any time of your day!', 0, 2.74, 355, 'can', 80),

-- 2. Half Pints
(2, 'Stir Stick Stout', 'stout', 6.0, 'stir_stick.jpg', 'Robust with ruby highlights and a generous addition of locally roasted fair trade, organic Ethiopian Yirgacheffe coffee', 0, 4.09, 473, 'can', 75),

(2, 'Little Scrapper IPA', 'IPA', 6.0, 'little_scrapper.jpg', 'A firm, toasted malt presence forms the background for all these hops lending balance so it’s not just a one note symphony', 0, 3.95, 473, 'can', 80),

(2, 'Bulldog Amber Ale', 'amber ale', 5.4, 'bulldog_amber.jpg', 'British crystal malts give this beer its distinctly deep amber body and nutty aroma that persist throughout the glass', 0, 2.32, 341, 'bottle', 72),

(2, 'St. James Pale Ale', 'pale ale', 4.8, 'st_james.jpg', 'St. James Pale Ale is a medium bodied blonde brew that’s soft on the palette with a malty finish', 0, 2.14, 341, 'bottle', 103),

-- 3. Kilter
(3, 'Juicii IPA', 'IPA', 7.0, 'juicii.jpg', 'A hoppy, tropical, and juicy New England IPA (7%) double dry-hopped with El Dorado, Vic Secret, lupulin-enriched Mosaic & Citra', 0, 4.35, 473, 'can', 140),

(3, 'Waves Juicy Pale Ale', 'pale ale', 5.0, 'waves_juicy.jpg', 'A tropical, breezy, and juicy New England Pale Ale (5%) dry-hopped with waves of El Dorado & Citra', 0, 4.09, 473, 'can', 150),

(3, 'Fog Machine Cashmere Double IPA', 'IPA', 8.0, 'for_machine.jpg', 'A super hoppy single-hop Double IPA brewed and double dry-hopped with 100% Cashmere!', 0, 4.89, 473, 'can', 80),

(3, 'Throwback West Coast IPA', 'IPA', 7.0, 'throwback_IPA.jpg', 'A dank & hoppy West Coast IPA (7%) brewed with Golden Promise malt and aggressively dry-hopped with Centennial, Cryo Simcoe, and Cryo Amarillo', 0, 4.59, 473, 'can', 100),

-- 4. Little Brown Jug
(4, '1919 Belgian Pale Ale', 'pale ale', 5.0, '1919_IPA.jpg', '1919 is an easy-drinking everyday Belgian Pale Ale', 0, 2.40, 473, 'can', 93),

(4, 'Golden Ale', 'specialty ale', 6.5, 'golden_ale.jpg', 'A CRISP GOLDEN ALE IN OUR BELGIAN STYLE MADE WITH MANITOBA HONEY', 0, 2.40, 473, 'can', 114),

(4, 'Summer Lager', 'lager', 5.0, 'summer_lager.jpg', 'A SUMMER LAGER WITH A REFRESHING OUTLOOK', 0, 2.40, 473, 'can', 104),

(4, 'Belgian IPA', 'IPA', 6.5, 'belgian_ipa.jpg', 'ENGINEERED TO SHOWCASE NEW WORLD HOPS IN OUR BELGIAN STYLE', 0, 2.40, 473, 'can', 120),

-- 5. Fort Garry
(5, 'Lime Seltzer', 'seltzer', 5.0, 'lime_seltzer.jpg', 'Lime flavour Seltzer', 0, 3.59, 473, 'can', 72),

(5, 'Cranberry Crush Radler', 'radler', 3.5, 'cranberr_cruch_radler.jpg', 'A beautiful pink-hued, effervescent radler, infused with tart cranberry juice', 0, 3.79, 473, 'can', 88),

(5, 'Portage and Main IPA', 'IPA', 6.5, 'portagage_and_main.jpg', 'All aboard with this west coast-style IPA, featuring high-quality malted barley and whole west coast and Manitoba flower hops', 0, 3.99, 473, 'can', 110),

(5, 'Northern Lite', 'other', 4.0, 'northern_lite.jpg', 'Northern Lite is a clean, crisp beer which finishes dry. A perfect beach or cabin companion. Made using 100% barley we strive to create the best tasting light beer possible', 0, 2.98, 473, 'can', 103);

-- Create table users
CREATE TABLE users (
id INT NOT NULL AUTO_INCREMENT,
last_name VARCHAR(255) NOT NULL,
first_name VARCHAR(255) NOT NULL,
username VARCHAR(255) NOT NULL,
password VARCHAR(255) NOT NULL,
phone_number VARCHAR(255),
email VARCHAR(255) NOT NULL,
street VARCHAR(255),
city VARCHAR(255),
postal_code VARCHAR(255),
province VARCHAR(255),
country VARCHAR(255),
created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (id)
);

ALTER TABLE users MODIFY phone_number VARCHAR(255);

-- Insert value in users
INSERT INTO
users
(id, last_name, first_name, username, password, phone_number, email, street, city, postal_code, province, country)
VALUES
(1, 'Henry', 'Thierry', 'Emirates', 'Fassword', 1111111111, 'striker@example.com', '123 Arsenal Street', 'Winnipeg', 'R3B 2E9', 'Manitoba', 'Canada'),
(2, 'Pirlo', 'Andrea', 'OldLady', 'Iassword', 2222222222, 'regista@example.com', '234 Juventus Street', 'Winnipeg', 'R3B 2E8', 'Manitoba', 'Canada'),
(3, 'Carragher', 'Jamie', 'CenterBack', 'Lassword', 3333333333, 'kop@example.com', '345 Anfield Street', 'Winnipeg', 'R3B 2E7', 'Manitoba', 'Canada'),
(4, 'Neville', 'Gary', 'Fullback', 'Massword', 4444444444, 'red_devil@example.com', '456 Old Trafford Street', 'Winnipeg', 'R3B 2E6', 'Manitoba', 'Canada'),
(5, 'Busquets', 'Sergio', 'anchorman', 'Sassword', 5555555555, 'barcelona@example.com', '413 Camp Nou Street', 'Winnipeg', 'R3B 2E5', 'Manitoba', 'Canada');

-- Create table reviews
CREATE TABLE reviews (
id INT NOT NULL AUTO_INCREMENT,
beer_id INT NOT NULL,
user_id INT NOT NULL,
title VARCHAR(255),
description VARCHAR(255),
star_num INT(5),
created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (id),
FOREIGN KEY (beer_id) REFERENCES beer(id),
FOREIGN KEY (user_id) REFERENCES users(id)
);

-- Insert values
INSERT INTO
reviews (beer_id, user_id, title, description, star_num)
VALUES
(1, 1, 'Interesting', 'Bluebeary Ale was one of the first craft beers I had ever tried. In my opinion, it is more of a summer beer as it is fruity yet refreshing. I prefer bottle over can, but I stole this from Shans beer fridge so can’t complain. Cheers', 3),

(14, 2, 'Honey Taste!', 'I really enjoyed this brew! Great work! Loved the honey taste', 4),

(20, 3, 'Taste of Winnipeg', 'Clean, light, non descript beer', 3),

(13, 4, 'Belgian Classic', 'Nice clean crisp and modestly sweet with a definite malt flavour', 5),

(8, 5, 'Great Pale Ale!', 'I can’t believe I’ve never checked this one in', 4);

```

## Step 5

Create a MySQLdump to backup the database. The dump file should be saved in your
repository as yourname_capstone.sql

sudo mysqldump project > DongHyopHA_capstone.sql
