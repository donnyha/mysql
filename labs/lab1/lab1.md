# Lab 1

## Step 1

Winnitap is a site that displays different breweries in Winnipeg. It will showcase information such as beer selection and the locations of each brewery. There will be a functionality where a Google Map API will be integrated to let the user know where the closest local brewaries are near them.

Unauthenticated guests will be able to see what kind of beer each breweries produce as well as their locations.Regular authenticated users will be able to leave reviews and an admin user will be able to remove any innapropriate conentents on the site.

## Step 2

### Entities

1. users
2. beers
3. brewaries
4. reviews
5. visits
6. clicks

## Step 3

### Attributes - Beer

1. id
2. name
3. type
4. description
5. rating
6. brewary_name
7. brewary_location
8. price
9. created_at
10. updated_at
11. views

## Step 4

### Schema

beers(id, name, type, brewary_name, brewary_location, price)

### Predicate

beers are identified by the integer (id),
name of the beer in short variable length text(name),
type of beer in short variable length text (type),
description of the beer in long text (description),
rating of the beer out of five starts (rating)
name of the brewary it is from in short variable length text (brewary_name),
where the brewary is located at in short variable length text (brewary_location),
price of the beer in a floating number (price),
when the information was created in date format(created_at),
when the information was updated in date format(updated_at),
how many times the users view the product in integer (views)
