# Physical Data Model

## Legend

PK - Primary Key
FK - Foreign Key
required - Can't be null
default - Can't be null, defaults to specific value


movies x
    id - integer (PK)
    title - string max length 255 required
    year - integer required
    studio_id - integer and (FK) referencing Studie table
    description - text field for long description
    number_of_rentals - integer
    created_at - date field

genremovie x
    movie_id - integer (PK, FK)
    genre_id - integer (PK, FK)

genres x
    id - integer (PK)
    name - string max length 255

actormovie x 
    movie_id - integer (PK, FK)
    actor_id - integer (PK, FK)

actors x
    id - integer (PK)
    last_name - string max length 255
    given_names - string max length 255
    age - integer
    status - active or inactive default active
    created_at - date field

studios x
    id - integer (PK)
    name - string max length 255 required
    country - string max length 255 required
    created_at - date field

directormovies x
    movie_id - integer (PK, FK)
    director_id - integer (PK, FK)

directors x
    id - integer (PK)
    last_name - string max length 255 required
    given_names - string max length 255 required
    age - integer
    status - active or inactive default active

