-- CREATE ALL PRIMARY TABLES

DROP TABLE IF EXISTS genre;
create table genre (
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
name VARCHAR(255)
);

DROP TABLE IF EXISTS movies;
create table movies (
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
title VARCHAR(255),
year INT NOT NULL,
studio_id INT,
description TEXT,
number_of_rentals INT DEFAULT 0,
created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);

DROP TABLE IF EXISTS actors;
create table actors (
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
last_name VARCHAR(255),
given_names VARCHAR (255),
age INT,
status enum('active', 'inactive') DEFAULT 'active',
created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);

DROP TABLE IF EXISTS directors;
create table directors (
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
last_name VARCHAR(255),
given_names VARCHAR (255),
age INT,
status enum('active', 'inactive') DEFAULT 'active',
created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);

DROP TABLE IF EXISTS studios;
create table studios (
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
name VARCHAR(255),
country VARCHAR(255),
created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- CREATE ALL INTERSECT TABLES

DROP TABLE IF EXISTS movie_actor;
create table movie_actor (
movie_id INT NOT NULL,
actor_id INT NOT NULL,
PRIMARY KEY (movie_id, actor_id)
);

DROP TABLE IF EXISTS movie_director;
create table movie_director (
movie_id INT NOT NULL,
director_id INT NOT NULL,
PRIMARY KEY (movie_id, director_id)
);

DROP TABLE IF EXISTS movie_genre;
create table movie_genre (
movie_id INT NOT NULL,
genre_id INT NOT NULL,
PRIMARY KEY (movie_id, genre_id)
);

-- INSERT INTO ALL PRIMARY TABLES

INSERT INTO movies
(title, year, studio_id, description)
VALUES
('2001: A Space Odyssey', 1967, 1, 'A great movie.'),
('Star wars', 1975, 2, 'Space on a budget with flashing swords.'),
('The French Connection', 1972, 2, 'Nasty cop chases nasty drug dealers in New York.'),
('Night of the Living Dead', 1971, 3, 'When there is no more room in Hell, the dead shall walk the earth.'),
('Ad Astra', 2020, 4, 'Sullen astronaut heads into space to find his missing father.'),
('The Martian', 2018, 4, 'Robinson Crusoe on Mars remake');


INSERT INTO actors 
(last_name, given_names, age)
VALUES
('Dulea', 'Keirr', 30),
('Lockwood', 'Gary', 31),
('Rain', 'Douglas', 38),
('Fisher', 'Carrie', 22),
('Hammil', 'Mark', 23),
('Ford', 'Harrison', 30),
('Hackman', 'Gene', 38),
('Scheider', 'Roy', 35),
('Jones', 'Duane', 27),
('Oday', 'Judith', 26),
('Pitt', 'Bradd', 41),
('Jones', 'Tommy Lee', 67);


INSERT INTO directors
(last_name, given_names, age)
VALUES
('Kubrick', 'Stanley', 65),
('Lucas', 'George', 64),
('Friedkin', 'William', 75),
('Romero', 'George', 72),
('Gray', 'James', 38);


INSERT INTO studios
(name, country)
VALUES
('MGM', 'USA'),
('Universal', 'USA'),
('Hammer', 'England'),
('Lucas Film', 'USA'),
('Ealing', 'England'),
('Paramount', 'USA'),
('RKO', 'USA');

INSERT INTO genre
(name)
VALUES
('Science Fiction'),
('Horror'),
('Drama'),
('Romance'),
('Thriller'),
('Fantasy'),
('Political');

INSERT INTO movie_actor
values
(1,1),(1,2),(1,3),
(2,4),(2,5),(2,6),
(3,7),(3,8),
(4,9),(4,10),
(5,11),(5,12);

INSERT INTO movie_director
values
(1,1),
(2,2),
(3,3),
(4,4),
(5,5);

INSERT INTO movie_genre
values
(1,1),(1,3),
(2,1),(2,4),
(3,5),
(4,2),(4,7),
(5,1),(5,3);

