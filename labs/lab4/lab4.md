# Lab 4 - Donny

## Step 1

Using the database you created in Lab 3, create the queries to return the following results. Queries and results should be cut/pasted into a markdown or sql file and saved in the lab folder:

1. A result that shows movie title and actors (two fields), grouped by title

```sql
SELECT
movies.title,
GROUP_CONCAT(actors.last_name) as actors
FROM
movies
JOIN movie_actor ON movies.id = movie_actor.movie_id
JOIN actors ON movie_actor.actor_id = actors.id
GROUP BY movies.title;
```

2. A result that shows actors and all the movies they’ve acted in (two fields), grouped by actor

```sql
SELECT
actors.last_name,
GROUP_CONCAT(movies.title) as movies
FROM
movies
JOIN movie_actor ON movies.id = movie_actor.movie_id
JOIN actors ON movie_actor.actor_id = actors.id
GROUP BY actors.last_name;
```

3. A result that shows all actors and the number of movies they’ve acted in (two fields), grouped by actor

```sql
SELECT
actors.last_name,
COUNT(movies.id) as num_movies
FROM
movies
JOIN movie_actor ON movies.id = movie_actor.movie_id
JOIN actors ON movie_actor.actor_id = actors.id
GROUP BY actors.last_name
ORDER BY num_movies DESC;
```

4. A result that shows all studios and all the movies they’ve made in (two fields), grouped by studio

```sql
SELECT
studios.name as studio,
GROUP_CONCAT(movies.title) as movies
FROM
movies, studios
WHERE
studios.id = movies.studio_id
GROUP BY studios.name;
```

5. A result that shows all movies, all actors in each movie, and the number of actors in each movie, ordered by the number of actors in each movie (three fields), grouped by movie

```sql
SELECT
movies.title as movies,
GROUP_CONCAT(DISTINCT actors.last_name) as actors,
COUNT(actors.id) as num_actors
FROM
movies
JOIN movie_actor ON movies.id = movie_actor.movie_id
JOIN actors ON movie_actor.actor_id = actors.id
GROUP BY movies.title;
```

6. Generate a report that shows the minimum, maximum, and average number of actors per movie (1 row, 3 fields), grouped by movie

```sql
SELECT
movies.title as movie,
MIN(COUNT(actors.id)) as min_num_actors,
MAX(COUNT(actors.id)) as max_num_actors,
AVG(COUNT(actors.id)) as avg_num_actors
FROM
movies
JOIN movie_actor ON movies.id = movie_actor.movie_id
JOIN actors ON movie_actor.actor_id = actors.id
GROUP BY movies.title;
```

7. A result that shows: movie, year, studio, director(s), actor(s), genre(s), grouped by movie title

```sql
SELECT
movies.title as movie,
movies.year as year,
studios.name as studio,
GROUP_CONCAT(DISTINCT directors.last_name) as director,
GROUP_CONCAT(DISTINCT actors.last_name) as actors,
GROUP_CONCAT(DISTINCT genre.name) as genre
FROM movies
JOIN studios ON movies.studio_id = studios.id
JOIN movie_actor ON movie_actor.movie_id = movies.id
JOIN actors ON actors.id = movie_actor.actor_id
JOIN movie_director ON movies.id = movie_director.movie_id
JOIN directors ON movie_director.director_id = directors.id
JOIN movie_genre ON movies.id = movie_genre.movie_id
JOIN genre ON movie_genre.genre_id = genre.id
GROUP BY movies.title;
```

8. A stored procedure that will execute the previous query

```sql
DROP PROCEDURE IF EXISTS movies.getInfo;
DELIMITER //
CREATE PROCEDURE lab4.getInfo()
BEGIN

SELECT
movies.title as movie,
movies.year as year,
studios.name as studio,
GROUP_CONCAT(DISTINCT directors.last_name) as director,
GROUP_CONCAT(DISTINCT actors.last_name) as actors,
GROUP_CONCAT(DISTINCT genre.name) as genre
FROM movies
JOIN studios ON movies.studio_id = studios.id
JOIN movie_actor ON movie_actor.movie_id = movies.id
JOIN actors ON actors.id = movie_actor.actor_id
JOIN movie_director ON movies.id = movie_director.movie_id
JOIN directors ON movie_director.director_id = directors.id
JOIN movie_genre ON movies.id = movie_genre.movie_id
JOIN genre ON movie_genre.genre_id = genre.id
GROUP BY movies.title;

END //
DELIMITER ;
```

## Step 2

Make a MySQL dump of your database and stored procedure.

```sql
sudo mysqldump --routines lab4 > lab4.sql
```
