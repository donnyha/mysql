# Lab 2

## Step 1

### Schema

Vetrinarian Pet Owner Directory

animals([id], name, type, date_of_birth, color, client_name, client_street, client_postalcode, client_phone, vaccinations)

**Business rules**

- Clients can own multiple animals
- Animals can have many vaccinations
- Vaccinations are given to multiple pets
- Clients can have more than one phone

## Step 2

- animals([id], name, type, date_of_birth, color, client_id)

- clients([id], client_name, client_street, client_postalcode, phone_id)

- phone([id], number)

- vaccination_record(animal_id, vaccination_id)

- vaccination([id], name)

## Step 3

Please see attached lab2_data_models.pdf
